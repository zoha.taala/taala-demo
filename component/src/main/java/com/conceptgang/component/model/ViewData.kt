package com.conceptgang.component.model

import android.content.Context
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import com.conceptgang.component.util.px
import java.security.InvalidParameterException
import java.util.*


typealias ViewCallback = (Int, ViewData, Any?)->Unit


sealed class ViewData {
    abstract val tag: Any?
    abstract val epoxyData: EpoxyData
}

data class TaalaViewData(
    val text: ViewString,
    override val tag: Any? = null,
    override val epoxyData: EpoxyData = EpoxyData(horizontal = 16.px, vertical = 8.px)

): ViewData()

