package com.conceptgang.app.data.repository

import android.content.SharedPreferences
import com.conceptgang.app.base.BaseRepository
import com.conceptgang.app.data.local.LocalDatabase
import com.conceptgang.app.data.remote.TaalaClient
import javax.inject.Inject

class TaalaRepository @Inject constructor (
    val taalaClient: TaalaClient,
    val localDatabase: LocalDatabase,
    val preferences: SharedPreferences
): BaseRepository() {

}