package com.conceptgang.app.page.home

import androidx.lifecycle.viewModelScope
import com.airbnb.mvrx.*
import com.conceptgang.app.MainApp
import com.conceptgang.app.base.BaseViewModel
import com.conceptgang.app.data.repository.TaalaRepository
import com.conceptgang.component.model.TaalaViewData
import com.conceptgang.component.model.ViewData
import com.conceptgang.component.model.ViewString
import kotlinx.coroutines.launch

data class HomeState(
    val views: Async<List<ViewData>> = Uninitialized
): MvRxState

class HomeViewModel (state: HomeState, val repository: TaalaRepository): BaseViewModel<HomeState>(state){

    companion object: MvRxViewModelFactory<HomeViewModel, HomeState>{
        override fun create(viewModelContext: ViewModelContext, state: HomeState): HomeViewModel? {

            val mainApp = viewModelContext.app<MainApp>()

            val repository = mainApp.repository

            return HomeViewModel(state, repository)
        }
    }


    fun loadViews(){
        setState { copy(views = Loading()) }

        viewModelScope.launch {
            try {
                // Do some work or downlaod data from server
                val views = listOf(
                    TaalaViewData(ViewString("Hello")),
                    TaalaViewData(ViewString("Taala")),
                    TaalaViewData(ViewString("ViewData")),
                    TaalaViewData(ViewString("CustomView")),
                    TaalaViewData(ViewString("Epoxy")),
                    TaalaViewData(ViewString("Epoxy")),
                    TaalaViewData(ViewString("Epoxy")),
                    TaalaViewData(ViewString("Epoxy")),
                    TaalaViewData(ViewString("Epoxy")),
                    TaalaViewData(ViewString("Epoxy")),
                    TaalaViewData(ViewString("Epoxy"))
                )
                setState { copy(views = Success(views)) }

            }catch (ex: Exception){
                setState { copy(views = Fail(ex)) }
            }
        }
    }

}