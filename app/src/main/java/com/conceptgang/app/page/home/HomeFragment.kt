package com.conceptgang.app.page.home

import com.airbnb.mvrx.*
import com.conceptgang.app.base.BaseEpoxyFragment
import com.conceptgang.component.model.ViewCallback
import com.conceptgang.component.model.ViewData

class HomeFragment : BaseEpoxyFragment(){

    private val viewModel by fragmentViewModel(HomeViewModel::class)

    override val viewCallback: ViewCallback  = { i: Int, viewData: ViewData, any: Any? ->

        // This will be fired whenever user click on of custom view
    }

    override fun invalidate() = withState(viewModel) { state ->

        when(val views = state.views){
            is Uninitialized -> {
                viewModel.loadViews()
            }

            is Loading -> {
                // show loading
            }

            is Success -> {
                setEpoxyData(views())
            }

            is Fail -> {
                // handle error
            }
        }

    }

}